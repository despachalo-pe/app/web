import {map} from 'ramda';
import {Divider, List} from '@material-ui/core';
import {useOptions} from 'hooks/permissions';
import {makeStyles} from '@material-ui/core/styles';
import OptionItem from './OptionItem';
import Profile from './Profile';

const useStyles = makeStyles((theme) => ({
  root: {
    width: theme.spacing(20),
    minHeight: '100vh',
    backgroundColor: theme.palette.primary.light,
    color: theme.palette.common.white,
  },
  divider: {
    margin: theme.spacing(0, 1.5),
  },
}));

function SideOptions() {
  const classes = useStyles();
  const options = useOptions('company');

  return (
    <div className={classes.root}>
      <Profile />
      <Divider className={classes.divider} />
      <List>
        {map(({id, ...packItem}) => (
          <OptionItem
            id={id}
            key={id}
            {...packItem}
            handleClick={() => {}}
          />
        ), options)}
      </List>
    </div>
  );
}

export default SideOptions;
