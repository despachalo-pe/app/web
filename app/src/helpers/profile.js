import {compose, head, split} from 'ramda';

export function firstName(names) {
  return compose(head, split(' '))(names);
}

export function getFirstLetter(names) {
  return head(names.toUpperCase());
}

export function initials({names, lastnames}) {
  return `${getFirstLetter(names)}${getFirstLetter(lastnames)}`;
}
