import {makeStyles} from '@material-ui/core/styles';

import Box from '@material-ui/core/Box';
import Container from '@material-ui/core/Container';

import Copyright from './Copyright';
import LoginForm from './LoginForm';
import LoginHeader from './LoginHeader';
import LoginOptions from './LoginOptions';
import BlankLayout from '../Layout/BlankLayout';

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  copyright: {
    marginTop: theme.spacing(2),
  },
}));

function Login() {
  const classes = useStyles();

  return (
    <BlankLayout title="Iniciar Sesión">
      <Container component="main" maxWidth="xs">
        <Box mt={8}>
          <div className={classes.paper}>
            <LoginHeader />
            <LoginForm />
            <LoginOptions />
          </div>
          <Copyright className={classes.copyright} />
        </Box>
      </Container>
    </BlankLayout>
  );
}

export default Login;
