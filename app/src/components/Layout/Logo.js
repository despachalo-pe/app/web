import Image from 'next/image';

const Logo = (props) => (
  <Image
    width={32}
    height={32}
    alt="Logo"
    src="/logo.svg"
    {...props}
  />
);

export default Logo;
