import DashboardLayout from 'components/Layout/Dashboard';
import Typography from '@material-ui/core/Typography';
import {Box} from '@material-ui/core';
import {makeStyles} from '@material-ui/core/styles';
import NewUserButton from './NewUserButton';
import UsersTable from './Table';
1
const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
  },
  titleZone: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    margin: theme.spacing(0, 0, 4, 0),
  },
  title: {
    flexGrow: 1,
  },
}));

function Users() {
  const classes = useStyles();

  return (
    <DashboardLayout title="usuarios">
      <Box className={classes.root}>
        <Box className={classes.titleZone}>
          <Typography
            className={classes.title}
            variant="h6"
          >
            Usuarios del Sistema
          </Typography>
          <NewUserButton />
        </Box>

        <UsersTable />
      </Box>
    </DashboardLayout>
  );
}

export default Users;
