import BusinessIcon from '@material-ui/icons/Business';

const company = {
  id: 'company',
  label: 'Empresa y Usuarios',
  icon: <BusinessIcon />,
  url: 'empresa/usuarios',
  options: [
    {
      id: 'users',
      label: 'Usuarios',
      url: 'empresa/usuarios',
    },
    {
      id: 'roles',
      label: 'Roles',
      url: 'empresa/roles',
    },
    {
      id: 'centers',
      label: 'Centros',
      url: 'empresa/centros',
    },
  ],
};

const warehouse = {
  id: 'warehouse',
  label: 'Almacenamiento',
  icon: <BusinessIcon />,
  url: 'almacen/envios',
  options: [
    {
      id: 'sending',
      label: 'Envíos',
      url: 'almacen/envios',
    },
  ],
};

const packs = [
  company,
  warehouse,
];

export default packs;
