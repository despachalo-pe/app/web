import PropTypes from 'prop-types';
import {ListItem, ListItemIcon, Tooltip} from '@material-ui/core';
import {makeStyles} from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    margin: theme.spacing(0),
    padding: theme.spacing(0),
  },
  item: {
    display: 'flex',
    justifyContent: 'center',
    margin: theme.spacing(2, 0),
    padding: theme.spacing(0),
    color: theme.palette.common.white,
  },
}));

function PackItem({id, label, icon, url, handleClick}) {
  const classes = useStyles();

  return (
    <ListItem
      button
      key={id}
      alignItems="center"
      onClick={handleClick}
      className={classes.root}
    >
      <ListItemIcon className={classes.item}>
        <Tooltip
          title={label}
          placement="right"
        >
          {icon}
        </Tooltip>
      </ListItemIcon>
    </ListItem>
  );
}

PackItem.propTypes = {
  id: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  icon: PropTypes.node.isRequired,
  url: PropTypes.string.isRequired,
  handleClick: PropTypes.func.isRequired,
};

export default PackItem;
