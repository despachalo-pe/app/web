import Avatar from '@material-ui/core/Avatar';
import {Box} from '@material-ui/core';
import {firstName, initials} from 'helpers/profile';
import {makeStyles} from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    textAlign: 'center',
    margin: theme.spacing(2, 0),
  },
  avatar: {
    width: theme.spacing(6),
    height: theme.spacing(6),
    margin: theme.spacing(1),
    color: theme.palette.background.default,
    backgroundColor: theme.palette.action.active,
  },
}));

function Profile() {
  const classes = useStyles();
  const user = {names: 'Rocío', lastnames: 'Ventura Gamboa'};
  const nameInitials = initials(user);

  return (
    <Box className={classes.root}>
      <Avatar className={classes.avatar}>
        {nameInitials}
      </Avatar>
      <Typography>
        {`${firstName(user.names)} ${firstName(user.lastnames)}`}
      </Typography>
    </Box>
  );
}

export default Profile;
