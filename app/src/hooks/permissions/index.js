import {find, pipe, prop, propEq} from 'ramda';
import packs from './packs';

export function usePacks() {
  return packs;
}

export function useOptions(packId) {
  const packs = usePacks();
  return pipe(
    find(propEq('id', packId)),
    prop('options'),
  )(packs);
}
