import {map} from 'ramda';
import {List} from '@material-ui/core';
import {usePacks} from 'hooks/permissions';
import {makeStyles} from '@material-ui/core/styles';
import PackItem from './PackItem';

const useStyles = makeStyles((theme) => ({
  root: {
    width: theme.spacing(6),
    minHeight: '100vh',
    backgroundColor: theme.palette.primary.dark,
    color: theme.palette.common.white,
  },
}));

function SidePack() {
  const classes = useStyles();
  const packs = usePacks();

  return (
    <div className={classes.root}>
      <List>
        {map(({id, ...packItem}) => (
          <PackItem
            id={id}
            key={id}
            {...packItem}
            handleClick={() => {}}
          />
        ), packs)}
      </List>
    </div>
  );
}

export default SidePack;
