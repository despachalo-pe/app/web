import IconButton from '@material-ui/core/IconButton';
import React from 'react';

function MenuIcon() {
  return (
    <IconButton
      color="inherit"
      aria-label="menu"
    >
      <MenuIcon />
    </IconButton>
  );
}

export default MenuIcon;
