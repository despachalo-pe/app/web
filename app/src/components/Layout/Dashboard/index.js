import PropTypes from 'prop-types';
import {makeStyles} from '@material-ui/core/styles';
import {Box} from '@material-ui/core';
import Title from '../Title';
import SidePack from './SidePack';
import NavBar from './NavBar';
import SideOptions from './SideOptions';
import Content from './Content';

const useStyles = makeStyles((theme) => ({
  content: {
    display: 'flex',
    flexDirection: 'row',
  },
}));

const DashboardLayout = ({children, title}) => {
  const classes = useStyles();

  return (
    <>
      <Title title={title} />
      <Box>
        <NavBar />
        <Box className={classes.content}>
          <SidePack />
          <SideOptions />
          <Content>
            {children}
          </Content>
        </Box>
      </Box>
    </>
  );
};

DashboardLayout.propTypes = {
  children: PropTypes.node.isRequired,
  title: PropTypes.string.isRequired,
};

export default DashboardLayout;
