import PropTypes from 'prop-types';
import Head from 'next/head';

function Title({title}) {
  return (
    <Head>
      <title>
        {title}
        {' '}
        | Despáchalo PE 🚛
      </title>
    </Head>
  );
}

Title.propTypes = {
  title: PropTypes.string.isRequired,
};

export default Title;
