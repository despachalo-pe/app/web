import PropTypes from 'prop-types';
import {makeStyles} from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import {Divider} from '@material-ui/core';
import Breadcrumb from './Breadcrumb';

const useStyles = makeStyles((theme) => ({
  bread: {
    margin: theme.spacing(2, 0),
  },
  divider: {
    margin: theme.spacing(0, 0, 2, 0),
  },
}));

function Content({children}) {
  const classes = useStyles();

  return (
    <Container>
      <Breadcrumb className={classes.bread} />
      <Divider className={classes.divider} />
      {children}
    </Container>
  );
}

Content.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Content;
