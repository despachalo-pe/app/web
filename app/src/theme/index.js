import {createTheme} from '@material-ui/core/styles';
import {esES} from '@material-ui/core/locale';

const theme = createTheme({
  palette: {
    primary: {
      main: '#11A64F',
    },
    secondary: {
      main: '#4F4F4F',
    },
    background: {
      default: '#fff',
    },
  },
}, esES);

export default theme;
