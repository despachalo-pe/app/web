import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Link from '@material-ui/core/Link';

function handleClick(event) {
  event.preventDefault();
  console.info('You clicked a breadcrumb.');
}

function Breadcrumb({className}) {
  return (
    <Breadcrumbs className={className} aria-label="breadcrumb">
      <Link color="inherit" href="/" onClick={handleClick}>
        Empresa
      </Link>
      <Typography color="textPrimary">
        Usuarios
      </Typography>
    </Breadcrumbs>
  );
}

Breadcrumb.defaultProps = {
  className: '',
};

Breadcrumb.propTypes = {
  className: PropTypes.string,
};

export default Breadcrumb;
